/*
 File:ViewController.swift
 Project: PROG3230-Mobile Application Devlopment-A1 STL relators
 Programmers: Nathan Nickel, Brandon Davies, Lauren Machan, Hayden Taylor
 First Version: Oct 5, 2017
 Description: This file Contains all the data within the various fields of the application. out appliation is a realtor app that allows users to view and interact with house listings
 */

import UIKit
import Foundation
import CoreGraphics
import MapKit


/*
 NAME: ViewController: UIViewController
 PURPOSE: the purpose of the class is to fill all the applicable fields with their applicable data
 */
class ViewController: UIViewController {

    @IBOutlet var addressLabelL1: UILabel!
    @IBOutlet var addressLabelL2: UILabel!
    @IBOutlet var addressLabelL3: UILabel!
    
    @IBOutlet var priceLabelL1: UILabel!
    @IBOutlet var priceLabelL2: UILabel!
    @IBOutlet var priceLabelL3: UILabel!
    
    @IBOutlet var openHouseDateL1: UILabel!
    @IBOutlet var openHouseDateL2: UILabel!
    @IBOutlet var openHouseDateL3: UILabel!
    
    @IBOutlet var ratingLabel: UILabel!
    
    @IBOutlet var ratingSlider: UISlider!
    
    @IBOutlet var houseInformationTextViewL1: UITextView!
    @IBOutlet var   houseInformationTextViewL2: UITextView!
    @IBOutlet var houseInformationTextViewL3: UITextView!
    
    @IBOutlet var offerValue: UITextField!
    
    @IBOutlet var date: UIDatePicker!
    
    //Function: override func viewDidLoad()
    //Description: The is a event function for when the application first loads. all applicable fields are filled with their respective data here
    //Prarameters: void
    //Returns: void
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipe.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(swipe)
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        doubleTapRecognizer.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTapRecognizer)
        
        addressLabelL1?.text = "922 Bag End Street"
        addressLabelL2?.text = "1337 Dank Road"
        addressLabelL3?.text = "69 Bagshot Row"
        // 
        houseInformationTextViewL1?.text = "The entrance to Bag End is a perfectly round green door featuring a brass knob in the center. The entryway is a tube-shaped hall with paneled walls and a tiled floor, furnished with carpeting, polished chairs, and an abundance of pegs for the hats and coatsof many visitors. The tunnel continues into the hill with side doors that are also round. All of the rooms are on the same level – bedrooms, bathrooms, cellars, multiple pantries, wardrobes, kitchens, and dining rooms. The best rooms are those on the left side of the passage for they had deep-set round windows with a view of the garden and meadows beyond down to The Water."
        
        houseInformationTextViewL2?.text = "This five-bedroom circular house, which stands within the site of a former hilltop resevoir, fit the bill perfectly. A heafty $250,000 has just been slashed from the asking price, too. The 6000 sq ft of space includes a tunnel entrance, gallery, 2 kitchens, 4 bathrooms, a theater, pantry room, 69 bedrooms, gym, and double garage. Two acres of grounds overlook the town of Hobbiton."
        
        houseInformationTextViewL3?.text = "The four-bedroom hobbit hole features a spacious living room, multiple pantries, wardrobes, a large kitchen, two dining rooms, and five bathrooms. Each room includes handcrafted painted glass windows that overlook the area of Hobbiton. Also, the hole includes five acres of land and three outdoor gardens and a spacious backyard for your gardening and gathering needs. The hole has recently been decreased in price, so now is the time to buy it."
        
        priceLabelL1?.text = "Price: $420,360"
        priceLabelL2?.text = "Price: $1,000,101"
        priceLabelL3?.text = "Price: $151,200"
        
        openHouseDateL1?.text = "Open House Date: March 18, 2013"
        openHouseDateL2?.text = "Open House Date: August 25, 2015"
        openHouseDateL3?.text = "Open House Date: December 10, 2014"
    }
    
    //Function: @IBAction func submitOffer(sender: AnyObject)
    //Description: This is a event function that is called when the users presss the submit offer button. when clicked a message will appear notifiying the user that thier offer has been accpeted
    //Prarameters: <sender> <AnyObject> <object sending the event>
    //Returns: void
    @IBAction func submitOffer(sender: AnyObject) {
        //var offer = offerValue.text
        
        let currentLocale = NSLocale.current
        let decimalSeparator = currentLocale.decimalSeparator!
        
        let separatorExists = offerValue.text?.range(of: decimalSeparator)
        
        let num = Int(offerValue.text!)
        
        if (separatorExists != nil) || num != nil {
            print("The offer has been accepted.")
            offerValue.text = ""
            
            // get date
            let offerDate = date.date
            print(offerDate)
            
            let currentDate = Date()
            date.setDate(currentDate, animated: false)
            
            ratingSlider.value = 2.5
            ratingLabel.text = "Rating: 2"
        }else{
            let alert = UIAlertController(title: "Error", message: "Please enter a number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title:"Ok", style: .default, handler: {(action: UIAlertAction!) in alert.dismiss(animated: true, completion: nil)}))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }

    //Function: override func didReceiveMemoryWarning()
    //Description: if a memory error occurs then dispose of reasources that can be reloaded/recreated
    //Prarameters: void
    //Returns: void
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Function: @IBAction func sliderValueChanged(sender: UISlider)
    //Description: this is a event function that is called when the user modifies the value of the rating slider. updates the rating
    //Prarameters: <sender><UISlider><rating slider object>
    //Returns: void
    @IBAction func sliderValueChanged(sender: UISlider) {
        let value = Int(sender.value)        
        ratingLabel.text = "Rating: " + String(value)
    }

    //Function: override func viewDidLayoutSubviews()
    //Description: load subviews so the house descriptions are in the correct location
    //Prarameters: void
    //Returns: void
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        houseInformationTextViewL1?.setContentOffset(CGPoint.zero, animated: false)
        houseInformationTextViewL2?.setContentOffset(CGPoint.zero, animated: false)
        houseInformationTextViewL3?.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @objc func handleTap(gestureRecognizer: UITapGestureRecognizer)//handle double tap
    {
        let point = gestureRecognizer.location(in: self.houseInformationTextViewL1)
        
        let menu = UIMenuController.shared
        //becomeFirstResponder()
        let deleteItem = UIMenuItem(title: "Check Bids", action: #selector(checkBids))
        menu.menuItems = [deleteItem]
        menu.setTargetRect(CGRect(x: point.x,y: point.y, width:2,height:2), in: self.view)
        menu.setMenuVisible(true, animated: true)
    }
    @objc func checkBids()
    {
        let alert = UIAlertController(title: "Bids", message: "There are currently no bids on this property", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title:"Ok", style: .default, handler: {(action: UIAlertAction!) in alert.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func handleSwipe(gestureRecognizer: UISwipeGestureRecognizer)//handle swipe
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

